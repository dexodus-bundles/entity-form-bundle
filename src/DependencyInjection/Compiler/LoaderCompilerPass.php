<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\DependencyInjection\Compiler;

use Dexodus\EntityFormBundle\Attribute\EntityForm as EntityFormAttribute;
use Dexodus\EntityFormBundle\Service\EntityFormLoaderInterface;
use Exception;
use ReflectionAttribute;
use ReflectionClass;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class LoaderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $loaderClass = $container->getParameter('entity-form.loader_class');

        if (!$container->hasDefinition($loaderClass)) {
            $container->setDefinition($loaderClass, new Definition($loaderClass));
        }

        $container->setAlias(EntityFormLoaderInterface::class, $loaderClass);
        $loader = $container->findDefinition(EntityFormLoaderInterface::class);

        foreach ($container->getParameter('entity-form.mapping') as $mapping) {
            $potentialEntityFormPaths = scandir($mapping['dir']);

            if ($potentialEntityFormPaths === false) {
                throw new Exception("Directory '{$mapping['dir']}' not found for load entity forms");
            }

            $this->scanDirectoryAndAddEntityFormToLoader($mapping['dir'], $mapping['prefix'], $loader);
        }
    }

    private function scanDirectoryAndAddEntityFormToLoader(string $directory, string $prefix, Definition $loader): void
    {
        $entityForms = [];

        foreach (scandir($directory) as $path) {
            if ($path === '.' || $path === '..') {
                continue;
            }

            $filepath = $directory . '/' . $path;
            $class = $prefix . '\\' . str_replace('.php', '', $path);

            if (!class_exists($class)) {
                if (is_dir($filepath)) {
                    $this->scanDirectoryAndAddEntityFormToLoader($filepath, $prefix . '\\' . $path, $loader);
                }

                continue;
            }

            $entityFormAttributes = $this->getEntityFormAttributes($class);

            if (count($entityFormAttributes) === 0) {
                continue;
            }

            $entityForms[] = $class;
        }


        $loader->addMethodCall('setEntityForms', [array_unique($entityForms)]);
    }

    /** @return ReflectionAttribute[] */
    private function getEntityFormAttributes(string $class): array
    {
        $reflectionClass = new ReflectionClass($class);

        return $reflectionClass->getAttributes(EntityFormAttribute::class);
    }
}
