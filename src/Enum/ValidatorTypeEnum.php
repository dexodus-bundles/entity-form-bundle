<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\Enum;

enum ValidatorTypeEnum: string
{
    case JSEL = 'jsel';
}
