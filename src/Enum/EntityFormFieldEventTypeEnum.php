<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\Enum;

enum EntityFormFieldEventTypeEnum: string
{
    case JSEL = 'jesl';
}
