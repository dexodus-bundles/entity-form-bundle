<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\Enum;

enum EntityFormEventTypeEnum: string
{
    case JSEL = 'jesl';
}
