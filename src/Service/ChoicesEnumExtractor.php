<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\Service;

use Dexodus\EntityFormBundle\Attribute\Title;
use Dexodus\EntityFormBundle\Exception\MoreThenOneAttributeException;
use ReflectionEnum;

class ChoicesEnumExtractor
{
    public function extract(string $class): array
    {
        $reflectionEnum = new ReflectionEnum($class);
        $enumCases = $reflectionEnum->getCases();
        $choices = [];

        foreach ($enumCases as $enumCase) {
            $name = $enumCase->getBackingValue();

            $titleAttributes = $enumCase->getAttributes(Title::class);

            if (count($titleAttributes) > 1) {
                throw new MoreThenOneAttributeException($class, $enumCase->name, Title::class);
            }

            $title = isset($titleAttributes[0]) ? $titleAttributes[0]->newInstance()->value : $name;
            $choices[$name] = $title;
        }

        return $choices;
    }
}
