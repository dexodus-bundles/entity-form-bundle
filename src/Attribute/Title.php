<?php

declare(strict_types=1);

namespace Dexodus\EntityFormBundle\Attribute;

use Attribute;
use Dexodus\EntityFormBundle\Dto\EntityFormField as EntityFormFieldDto;

#[Attribute(Attribute::TARGET_CLASS_CONSTANT | Attribute::TARGET_PROPERTY)]
class Title extends AbstractFieldAttribute
{
    public function __construct(
        public readonly string $value,
    ) {
    }

    public function onAfterCreateField(EntityFormFieldDto $field, array $groups): EntityFormFieldDto
    {
        $field->title = $this->value;

        return $field;
    }
}
